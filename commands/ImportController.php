<?php

namespace app\commands;

use app\models\Plans;
use app\models\PlanProperties;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class ImportController extends Controller
{
    public function actionPlans()
    {
        $xml=simplexml_load_file(Yii::getAlias('@app').'/modules/plans.xml');
        foreach ($xml->result->ROWSET->ROW as $row){
            $model=new Plans();
            $model->plan_id=(int)$row->PLAN_ID;
            $model->plan_name=(string)$row->PLAN_NAME;
            $model->plan_group_id=(int)$row->PLAN_GROUP_ID;
            $model->active_from=date('Y-m-d', strtotime($row->ACTIVE_FROM));
            $model->active_to=date('Y-m-d' ,($row->ACTIVE_TO=='')?time():strtotime($row->ACTIVE_TO));
            $model->company_id=(int)$row->COMPANY_ID;
            $model->save();
        }
        echo 'save';
        return ExitCode::OK;
    }
    
    public function actionProperties()
    {
        $xml=simplexml_load_file(Yii::getAlias('@app').'/modules/plan_properties.xml');
        foreach ($xml->result->ROWSET->ROW as $row){
            $model=new PlanProperties ();
            $model->property_id=(int)$row->PROPERTY_ID;
            $model->property_type_id=(int)$row->PROPERTY_TYPE_ID;
            $model->active_from=date('Y-m-d', strtotime($row->ACTIVE_FROM));
            $model->active_to=date('Y-m-d' ,($row->ACTIVE_TO=='')?time():strtotime($row->ACTIVE_TO));
            $model->plan_id=(int)$row->PLAN_ID;
            $model->prop_value=(string)$row->PROP_VALUE;
            $model->save();
        }
        echo 'save';
        return ExitCode::OK;
    }
}
