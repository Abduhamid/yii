<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plans".
 *
 * @property int $plan_id
 * @property string|null $plan_name
 * @property int|null $plan_group_id
 * @property string|null $active_from
 * @property string|null $active_to
 * @property int|null $company_id
 */
class Plans extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plans';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plan_group_id', 'company_id'], 'integer'],
            [['active_from', 'active_to'], 'safe'],
            [['plan_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => 'Plan ID',
            'plan_name' => 'Plan Name',
            'plan_group_id' => 'Plan Group ID',
            'active_from' => 'Active From',
            'active_to' => 'Active To',
            'company_id' => 'Company ID',
        ];
    }
}
