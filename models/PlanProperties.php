<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plan_properties".
 *
 * @property int $property_id
 * @property int|null $property_type_id
 * @property string|null $active_from
 * @property string|null $active_to
 * @property int|null $plan_id
 * @property string|null $prop_value
 */
class PlanProperties extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plan_properties';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['property_type_id', 'plan_id'], 'integer'],
            [['active_from', 'active_to'], 'safe'],
            [['prop_value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'property_id' => 'Property ID',
            'property_type_id' => 'Property Type ID',
            'active_from' => 'Active From',
            'active_to' => 'Active To',
            'plan_id' => 'Plan ID',
            'prop_value' => 'Prop Value',
        ];
    }
}
