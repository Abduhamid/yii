<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%plans}}`.
 */
class m191123_165740_create_plans_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%plans}}', [
            'plan_id' => $this->primaryKey(),
            'plan_name'=>$this->string(),
            'plan_group_id'=>$this->integer(),
            'active_from'=>$this->date(),
            'active_to'=>$this->date(),
            'company_id'=>$this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%plans}}');
    }
}
