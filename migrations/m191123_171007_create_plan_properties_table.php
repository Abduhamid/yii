<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%plan_properties}}`.
 */
class m191123_171007_create_plan_properties_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%plan_properties}}', [
            'property_id' => $this->primaryKey(),
            'property_type_id'=>$this->integer(),
            'active_from'=>$this->date(),
            'active_to'=>$this->date(),
            'plan_id'=>$this->integer(),
            'prop_value'=>$this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%plan_properties}}');
    }
}
